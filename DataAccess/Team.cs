﻿using System.Collections.Generic;

namespace DataAccess
{
    public class Team : Entity
    {
        public string Name { get; set; }
        public virtual ICollection<Teammate> Teammates { get; set; }
    }
}